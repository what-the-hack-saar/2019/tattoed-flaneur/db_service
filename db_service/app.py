# -*- coding: utf-8 -*-

from flask import Flask, jsonify, request
from flask_headers import headers
import json
import os
import psycopg2

app = Flask(__name__)

cur = None


def query(cur, q='', schema_type='Event', limit=10):
        cur.execute('select e.data from names n, entities e where lower(n.name) like %s and n.id=e.id and e.type=%s limit(%s);', (q + '%', schema_type, limit))
        return [r[0] for r in cur.fetchall()]


def connect():
    db_connect = os.environ.get('DB_CONNECTION')
    if not db_connect:
        raise EnvironmentError('Missing DB_CONNECTION in enviroment. Please do "export DB_CONNECTION=\"host=myhost dbname=hackathon user=hackathon password=hackathon\"')
    con = psycopg2.connect(db_connect)
    cur = con.cursor()
    return cur


def run_service():
    app.run()


@app.route("/")
@headers({'Access-Control-Allow-Origin':'*'})
def hello():
    cur = connect()
    q = request.args.get('q')
    if not q:
        result = query(cur)
    else:
        result = query(cur, q=q.lower())
    return jsonify(result)



if __name__=='__main__':
    run_service()
